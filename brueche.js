System.register("app", ["stimulus"], function (exports_1, context_1) {
    "use strict";
    var stimulus_1, application;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (stimulus_1_1) {
                stimulus_1 = stimulus_1_1;
            }
        ],
        execute: function () {
            console.log("hallo");
            application = stimulus_1.Application.start();
            application.register("hello", HelloController);
            application.register("clipboard", ClipboardController);
        }
    };
});
