
const SVGNS = "http://www.w3.org/2000/svg";

const MAIN_COLOR = "#9a94bc";

const RADIUS = 80;


interface SvgGenerator {
	create() : SVGElement
}

/*
class Fraction {
	numerator: number;
	denominator: number;

	constructor(n: number, d: number) {
		this.numerator = n;
		this.denominator = d;
	}

	create(parent: SVGElement) {
		let blocks: Block[] = [
			new Block(this.numerator, MAIN_COLOR, "black"),
			new Block(this.denominator - this.numerator, "#f0f0f0", "black")
		];

//		let s = document.createElementNS(SVGNS, "svg");
		parent.setAttribute("height", "100");
		parent.setAttribute("width", "100");
		parent.setAttribute("viewBox", "-100 -100 200 200");

		for (let e of create_circle(RADIUS, blocks)) {
			parent.appendChild(e.create());
		}
	}
}*/


class Block {
	count: number;
	color: string;
	border: string;

	constructor(count: number, color: string, border: string) {
		this.count = count;
		this.color = color;
		this.border = border;
	}

}

class Line {
	x: number;
	y: number;
	color: string;

	constructor(x: number, y: number, color: string) {
		this.x = x;
		this.y = y;
		this.color = color;
	}

	create() : SVGElement {
		let line = document.createElementNS(SVGNS, "line");
		line.setAttribute("x1", "0");
		line.setAttribute("y1", "0");
		line.setAttribute("x2", this.x.toString());
		line.setAttribute("y2", this.y.toString());
		line.setAttribute("stroke", this.color);
		line.setAttribute("stroke-width", "1");
		return line;
	}
}

class Slice {
	color: string;
	border: string;
	path: string;

	constructor(start_x: number, start_y: number, target_x: number, target_y: number, radius: number, large: boolean, color: string, border: string) {
		let sweep = 0;
		this.color = color;
		this.border = border;
		this.path = `M 0,0\
        L ${start_x}, ${start_y}\
        A ${radius} ${radius} 0 ${large ? 1 : 0} ${sweep} ${target_x} ${target_y}\
        Z`;
	}

	create() : SVGElement[] {
		let path = document.createElementNS(SVGNS, "path");
		path.setAttribute("d", this.path);
		path.setAttribute("fill", this.color);
		path.setAttribute("stroke", this.border);
		path.setAttribute("stroke-width", "3");
		return [path];
	}
}


class CircleLayout {
	slices: Slice[];
	lines: Line[];

	constructor(radius: number, blocks: Block[]) {
		this.lines = [];
		this.slices = [];

		let slice_count: number = 0;

		for (let b of blocks) {
			slice_count += b.count;
		}

		let start_x = 0;
		let start_y = radius;

		let step = 2.0 * Math.PI / slice_count;

		let pos = 0;

		for (let b of blocks.reverse()) {
			let i = 1.0;

			while (i <= b.count) {
				this.lines.push(
					new Line(
						Math.sin(step * (pos + i)) * radius,
						Math.cos(step * (pos + i)) * radius,
						b.border
					)
				)
				i += 1.0;
			}

			pos += b.count;
			let target_x = Math.sin(step * pos) * radius;
			let target_y = Math.cos(step * pos) * radius;

			let large = (b.count >= slice_count / 2);

			this.slices.push(
				new Slice(start_x, start_y, target_x, target_y, radius, large, b.color, b.border)
			);
			start_x = target_x;
			start_y = target_y;
		}
	}
}

class FractionCircle {
	blocks: Block[];

	constructor(data: Array<number>, colors: Array<Array<string>>) {
		this.blocks = [];
		for (let idx in data) {
			let c = colors[idx];
			this.blocks.push(
				new Block(data[idx], c[0], c[1])
			)
		}
	}

	createSvg(x: number, y: number, size: number) : SVGSVGElement {
		let s = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		s.setAttribute("height", size.toString());
		s.setAttribute("width", size.toString());
		s.setAttribute("x", x.toString());
		s.setAttribute("y", y.toString());
		s.setAttribute("viewBox", "-100 -100 200 200");

		let layout = new CircleLayout(80, this.blocks);

		for (let slice of layout.slices) {
			for (let e of slice.create())  {
				s.appendChild(e);
			}
		}

		for (let line of layout.lines) {
			s.appendChild(line.create());
		}

		return s;
	}
}


class FractionCircles extends HTMLElement {
	circles: FractionCircle[];

	constructor() {
		super();

		let raw_circles = JSON.parse(this.getAttribute("circles")) as Array<Array<number>>;
		console.log(raw_circles);
		let colors = JSON.parse(this.getAttribute("colors")) as Array<Array<string>>;
		console.log(colors);

		this.circles = [];

		for (let b of raw_circles) {
			this.circles.push(
				new FractionCircle(b, colors)
			);
		}
	}

	connectedCallback() {
		let s = document.createElementNS("http://www.w3.org/2000/svg", "svg");

		const size = 150;
		s.setAttribute("height", size.toString());
		s.setAttribute("width", (this.circles.length*size).toString());

		let cur_x = 0;
		let cur_y = 0;

		for (let c of this.circles) {
			s.appendChild(c.createSvg(cur_x, cur_y, size));
			cur_x += size;
		}

		this.appendChild(s);
	}
}

customElements.define("fraction-circles", FractionCircles);