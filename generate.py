import svgwrite
import math

from jinja2 import Environment, FileSystemLoader, select_autoescape

env = Environment(
    loader=FileSystemLoader('.'),
    autoescape=select_autoescape(['html', 'xml'])
)

html = env.get_template("index.jinja2")

color = "#9a94bc"

radius = 80

current_file = 0

def render_circle(bruch):
    active, total = bruch

    blocks = [ (active, color, "black"), (total-active, "#f0f0f0", "#e0e0e0")]
    global current_file
    current_file += 1
    filename = f"circle-{current_file}.svg"
    dwg = svgwrite.Drawing(filename, viewBox="-100 -100 200 200")
    for x in create_circle(radius, blocks):
        dwg.add(x)
    dwg.save()
    return filename

env.globals["render_circle"] = render_circle


def render_rect(blocks):
    global current_file
    current_file += 1
    filename = f"rect-{current_file}.svg"

    row_sizes = set()
    for row in blocks:
        row_sizes.add(sum((s for s,_ in row)))
    if len(row_sizes) != 1:
        raise RuntimeError(f"Rows have different lengths: {row_sizes}.")

    width = next(iter(row_sizes))
    height = len(blocks)

    cx = 160.0 / width
    cy = 160.0 / height

    pos_x = -80
    pos_y = -80

    dwg = svgwrite.Drawing(filename, viewBox="-100 -100 200 200")
    for row in blocks:
        for size, color in row:
            dwg.add(dwg.rect((pos_x,pos_y), ((size * cx), cy),
                    fill=colors[color],
                    stroke="black",
                    stroke_width="3",
                    ))
            for i in range(1,size):
                dwg.add(dwg.line(
                    (pos_x + (i*cx),pos_y),
                    (pos_x + (i*cx),pos_y + cy),
                    stroke_width="1", stroke="black"))
            pos_x += size * cx
        pos_x = -80
        pos_y += cy
    dwg.save()
    return filename


env.globals["render_rect"] = render_rect




def _slice(start_x, start_y, target_x, target_y, radius, large, color, border):
    sweep = 0
    p = f"""M 0,0
        L {start_x}, {start_y}
        A {radius} {radius} 0 {large} {sweep} {target_x} {target_y}
        Z"""
    return svgwrite.path.Path(p,
        fill=color,
        stroke=border,
        stroke_width="3",
    )



def create_circle(radius, blocks):
    slices = sum((s for s, _, _ in blocks))
    start_x, start_y = 0, radius

    step = 2.0 * math.pi / slices

    pos = 0

    for size, color, border in blocks[::-1]:
        if type(color) is int:
            color = colors[color]
        lines = []
        for i in range(1,size):
            lines.append((
                math.sin(step * (pos+i)) * radius,
                math.cos(step * (pos+i)) * radius
                ))

        pos += size
        target_x = math.sin(step * pos) * radius
        target_y = math.cos(step * pos) * radius

        if size >= slices / 2:
            large = 1
        else:
            large = 0

        yield _slice(start_x, start_y, target_x, target_y, radius, large, color, border)

        for x,y in lines:
            yield svgwrite.shapes.Line((0,0),(x,y), stroke_width="1", stroke="black")

        start_x, start_y = target_x, target_y


with open("index.html","w") as f:
    f.write(html.render())
