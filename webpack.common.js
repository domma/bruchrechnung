const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: path.resolve(__dirname,  './src/index.ts'),
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  plugins: [
     new CleanWebpackPlugin(),
     new HtmlWebpackPlugin({
	     template: path.resolve(__dirname, "./src/templates/index.html")
     })
  ],
  output: {
    filename: 'brueche.[chunkhash].js',
    path: path.resolve(__dirname, 'dist'),
  },
};
